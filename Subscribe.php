<?php
/**
* @package     
* @version
**/

namespace Najoleari\As400\Controller\Ajax;

use Magento\Framework\Controller\Result\JsonFactory;
use Najoleari\As400\Model\Mail\TransportBuilder;
use Zend_Mail;
use Zend_Mime;
use Zend_Mime_Part;

class Subscribe extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;
    protected $transportBuilder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        JsonFactory $resultJsonFactory,
        TransportBuilder $transportBuilder)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->transportBuilder = $transportBuilder;
    }


    public function execute()
    {
        $post = $this->getRequest()->getParams();

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/devraj2013.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $logger->info('enter in suscribe contrpller');

        // FTP server details
        $ftp_host   = "84.253.181.135";
        $ftp_username = "ecwakyftp";
        $ftp_password = "?34+ZY,8";

        // Open ftp connection
        $conn_id = ftp_connect($ftp_host) or die("Couldn't connect to $ftp_host");
        // Connecting to ftp server
        if(@ftp_login($conn_id, $ftp_username, $ftp_password)) {
            $logger->info('ftp connected');
            ftp_pasv($conn_id, true); // <-- important: set the passive mode
        }
        else {
            $logger->info('ftp not connected');
            die;
        }

        
        /** @var \Magento\Framework\App\ObjectManager $om */
        $om = \Magento\Framework\App\ObjectManager::getInstance();

        /** @var \Magento\Sales\Model\ResourceModel\Product\CollectionFactory $orderCollection */
        $orderCollection = $om->create('Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
        
        // Recover the roder with status Pending (COD) o processing (Payed) > 2018-02-28 12:00:00
        $collection = $orderCollection->create()
                                      ->addFieldToFilter('created_at', array('gt' => '2019-01-01 01:00:00'))
                                      ->addFieldToFilter('status', array('in' => array('processing', 'pending', 'as_400', 'complete')));

       
        // ----------------------------------------
        // SEND ORDERS
        // ----------------------------------------
        // For each order, create an xml file to push in the ftp
        foreach ($collection as $order){

            $xmlOutput = "<?xml version='1.0' encoding='utf-8'?>\n";
            $xmlOutput .= "<Orders>\n";
            
            $order_prefix = ($order->getStoreId() == 2) ? "EN" : "IT";
            $billingAddress = $order->getBillingAddress();
            $shippingAddress = $order->getShippingAddress();
            
            $xmlOutput .= "<Order>\n";

            
            // Dati ordine e id utente
            // ----------------------------------------
            $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
            if ($order->getCustomerIsGuest()) {
                $xmlOutput = $xmlOutput . "<Customer_Code/>\n";
            }
            else {
                $xmlOutput = $xmlOutput . "<Customer_Code>" . $order->getCustomerId() . "</Customer_Code>\n";
            }
            
            $date = date_create($order->getCreatedAt());
            $xmlOutput = $xmlOutput . "<Order_Date>" . date_format($date,"d/m/Y H:i:s") . "</Order_Date>\n";

            $xmlOutput = $xmlOutput . "<Order_Currency_Iso>eur</Order_Currency_Iso>\n";
            $xmlOutput = $xmlOutput . "<Order_Currency_Symbol>E</Order_Currency_Symbol>\n";
            $xmlOutput = $xmlOutput . "<Order_Language>" . $order_prefix . "</Order_Language>\n"; // codice lingua iso2 (IT)


            // Costi
            // ----------------------------------------
            $xmlOutput = $xmlOutput . "<Order_Subtotal>" . str_replace(".", ",", $order->getSubtotal()."") . "</Order_Subtotal>\n"; // subtotale articoli
            $xmlOutput = $xmlOutput . "<Order_Discount>" . str_replace(".", ",", $order->getDiscountAmount()."") . "</Order_Discount>\n"; // importo sconto (sempre importo)
            // Costi di spedizione
            $xmlOutput = $xmlOutput . "<Order_Shipment_Cost>" . str_replace(".", ",", $order->getShippingAmount()."") . "</Order_Shipment_Cost>\n"; // Costi spedizione
            $cashCostTxt = ($order->getPayment()->getMethodInstance()->getCode() == "cashondelivery") ? "3,5" : "0,0";
            $cashCost = ($order->getPayment()->getMethodInstance()->getCode() == "cashondelivery") ? 3.5 : 0.0;
            $xmlOutput = $xmlOutput . "<Order_Cash_Cost>" . $cashCostTxt . "</Order_Cash_Cost>\n"; // <-- Costi di Contrassegno
            // Totale
            $xmlOutput = $xmlOutput . "<Order_Total>" . str_replace(".", ",", $order->getGrandTotal() + $cashCost ."") . "</Order_Total>\n";
            // Coupon
            if (!empty($order->getCouponCode())) {
                $xmlOutput = $xmlOutput . "<Order_Coupon_Code>" . $order->getCouponCode() . "</Order_Coupon_Code>\n";
            }
            else {
                $xmlOutput = $xmlOutput . "<Order_Coupon_Code/>\n";
            }
            $xmlOutput = $xmlOutput . "<Order_Coupon_Value>" . str_replace(".", ",", $order->getDiscountAmount()."") . "</Order_Coupon_Value>\n";
            

            // Metodo di pagamento
            // ----------------------------------------
            if ($order->getPayment()->getMethodInstance()->getCode() == "cashondelivery") {
                // Cash on delivery
                $xmlOutput = $xmlOutput . "<Payment_Method_Code>CS</Payment_Method_Code>\n";
                $xmlOutput = $xmlOutput . "<Payment_Method_Name>Contrassegno</Payment_Method_Name>\n";
                $xmlOutput = $xmlOutput . "<Payment_Auth/>\n";
                $xmlOutput = $xmlOutput . "<Payment_Reference/>\n";
            }
            else if ($order->getPayment()->getMethod() == "monetaweb2") {
                // MonetaWeb
                $setefiInfo = $order->getPayment()->getAdditionalInformation();
                $paymentId = $setefiInfo["monetaweb2_payment_id"];            
                $securityToken = $setefiInfo["monetaweb2_payment_security_token"];
                
                $xmlOutput = $xmlOutput . "<Payment_Method_Code>AT</Payment_Method_Code>\n";
                $xmlOutput = $xmlOutput . "<Payment_Method_Name>Carta di credito</Payment_Method_Name>\n";
                $xmlOutput = $xmlOutput . "<Payment_Auth>" . $securityToken . "</Payment_Auth>\n";
                $xmlOutput = $xmlOutput . "<Payment_Reference>" . $paymentId . "</Payment_Reference>\n";
            }
            else if ($order->getPayment()->getMethod() == "monetaweb2mybank") {
                // Bonifico MyBank
                $setefiInfo = $order->getPayment()->getAdditionalInformation();
                $paymentId = $setefiInfo["monetaweb2_payment_id"];            
                $securityToken = $setefiInfo["monetaweb2_payment_security_token"];
                
                $xmlOutput = $xmlOutput . "<Payment_Method_Code>BD</Payment_Method_Code>\n";
                $xmlOutput = $xmlOutput . "<Payment_Method_Name>Bonifico</Payment_Method_Name>\n";
                $xmlOutput = $xmlOutput . "<Payment_Auth>" . $securityToken . "</Payment_Auth>\n";
                $xmlOutput = $xmlOutput . "<Payment_Reference>" . $paymentId . "</Payment_Reference>\n";
            }
            else if ($order->getPayment()->getMethod() == "paypal_express") {
                // Paypal
                $paymentId = $order->getPayment()->getLastTransId();
                
                $xmlOutput = $xmlOutput . "<Payment_Method_Code>PP</Payment_Method_Code>\n";
                $xmlOutput = $xmlOutput . "<Payment_Method_Name>Paypal</Payment_Method_Name>\n";
                $xmlOutput = $xmlOutput . "<Payment_Auth/>\n";
                $xmlOutput = $xmlOutput . "<Payment_Reference>" . $paymentId . "</Payment_Reference>\n";
            }
            $xmlOutput = $xmlOutput . "<Payment_Status_Code>AT</Payment_Status_Code>\n";
            $xmlOutput = $xmlOutput . "<Payment_Status_Name>Autorizzato</Payment_Status_Name>\n";


            // Status
            // ----------------------------------------
            $xmlOutput = $xmlOutput . "<Elaboration_Status_Code>IP</Elaboration_Status_Code>\n";
            $xmlOutput = $xmlOutput . "<Elaboration_Status_Name>In preparazione</Elaboration_Status_Name>\n";


            // Metodo Spedizione
            // ----------------------------------------
            $shippingMethod = "";
           
            if (isset($shippingAddress) && !empty($shippingAddress) && $shippingAddress->getCountryId() != "IT") {
                $shippingMethod = "International";
            }
            else {
                
                if (!empty($order->getCouponCode()) ) {
                    $couponCode = $order->getCouponCode();
                    if ( strtolower($couponCode) == strtolower("COCCOLEDINAJ") ) {
                        $shippingMethod = "Express";  
                    }
                    else {
                        if ($order->getShippingMethod() == "flatrate_flatrate") {
                            $shippingMethod = "Express";
                        }
                        else if ($order->getShippingMethod() == "freeshipping_freeshipping") {
                            $shippingMethod = "Free";
                        }
                        else {
                            $shippingMethod = "Standard";
                        }
                    }
                }
                else {
                    if ($order->getShippingMethod() == "flatrate_flatrate") {
                        $shippingMethod = "Express";
                    }
                    else if ($order->getShippingMethod() == "freeshipping_freeshipping") {
                        $shippingMethod = "Free";
                    }
                    else {
                        $shippingMethod = "Standard";
                    }
                }
            }
            $xmlOutput = $xmlOutput . "<Shipment_Method_Code>" . $shippingMethod . "</Shipment_Method_Code>\n";
            $xmlOutput = $xmlOutput . "<Shipment_Method_Name>" . $order->getShippingDescription() . "</Shipment_Method_Name>\n";


            // Unused fields
            // ----------------------------------------
            $xmlOutput = $xmlOutput . "<Shipment_Tracking_Code/>\n";
            $xmlOutput = $xmlOutput . "<Shipment_Notes/>\n";

            
            // Dati Utente
            // ----------------------------------------
            if ($order->getCustomerIsGuest()) {
                if(isset($shippingAddress) && !empty($shippingAddress)) {
                    $firstname = $shippingAddress->getFirstname();
                    $lastname = $shippingAddress->getLastname();
                }
            }
            else {
                $firstname = $order->getCustomerFirstname();
                $lastname = $order->getCustomerLastname();
            }

            $xmlOutput = $xmlOutput . "<Customer_Reference>" . $lastname."|".$firstname . "</Customer_Reference>\n";
            $xmlOutput = $xmlOutput . "<Customer_Name>" . $firstname . "</Customer_Name>\n";
            $xmlOutput = $xmlOutput . "<Customer_Surname>" . $lastname . "</Customer_Surname>\n";
            $xmlOutput = $xmlOutput . "<Customer_Company/>\n";
            $xmlOutput = $xmlOutput . "<Customer_Fiscal_Code/>\n";
            $xmlOutput = $xmlOutput . "<Customer_Vat_Number/>\n";
            $xmlOutput = $xmlOutput . "<Customer_Email>" . $order->getCustomerEmail() . "</Customer_Email>\n";
            $xmlOutput = $xmlOutput . "<Customer_Phone/>\n";

            if ($order->getCustomerIsGuest()) {
                $xmlOutput = $xmlOutput . "<Customer_Mobile/>\n";
            }
            else {
                $xmlOutput = $xmlOutput . "<Customer_Mobile>" . $order->getCustomerTelephone() . "</Customer_Mobile>\n";
            }
            $xmlOutput = $xmlOutput . "<Customer_Address/>\n";
            $xmlOutput = $xmlOutput . "<Customer_Zip/>\n";
            $xmlOutput = $xmlOutput . "<Customer_City/>\n";
            $xmlOutput = $xmlOutput . "<Customer_Province_Code/>\n";
            $xmlOutput = $xmlOutput . "<Customer_Province_Name/>\n";
            $xmlOutput = $xmlOutput . "<Customer_Country_Code/>\n";
            $xmlOutput = $xmlOutput . "<Customer_Country_Name/>\n";


            // Dati Fatturazione
            if(isset($billingAddress) && !empty($billingAddress)) {
                // ----------------------------------------
                $xmlOutput = $xmlOutput . "<Billing_Reference>" . $billingAddress->getLastname() . "|" . $billingAddress->getFirstname() . "</Billing_Reference>\n";
                $xmlOutput = $xmlOutput . "<Billing_Name>" . $billingAddress->getFirstname() . "</Billing_Name>\n";
                $xmlOutput = $xmlOutput . "<Billing_Surname>" . $billingAddress->getLastname() . "</Billing_Surname>\n";
                $xmlOutput = $xmlOutput . "<Billing_Company>" . $billingAddress->getCompany() . "</Billing_Company>\n";
                if ($billingAddress->getCompany() != "") {
                    $xmlOutput = $xmlOutput . "<Billing_Fiscal_Code/>\n";
                    $xmlOutput = $xmlOutput . "<Billing_Vat_Number>" . $billingAddress->getTaxCode() . "</Billing_Vat_Number>\n";
                }
                else {
                    $xmlOutput = $xmlOutput . "<Billing_Fiscal_Code>" . $billingAddress->getTaxCode() . "</Billing_Fiscal_Code>\n";
                    $xmlOutput = $xmlOutput . "<Billing_Vat_Number/>\n";
                }
                $xmlOutput = $xmlOutput . "<Billing_Email>" . $order->getCustomerEmail() . "</Billing_Email>\n";
                $xmlOutput = $xmlOutput . "<Billing_Phone/>\n";
                $xmlOutput = $xmlOutput . "<Billing_Mobile>" . $billingAddress->getTelephone() . "</Billing_Mobile>\n";
                $xmlOutput = $xmlOutput . "<Billing_Address>" . $billingAddress->getStreet()[0] . "</Billing_Address>\n";
                $xmlOutput = $xmlOutput . "<Billing_Zip>" . $billingAddress->getPostcode() . "</Billing_Zip>\n";
                $xmlOutput = $xmlOutput . "<Billing_City>" . $billingAddress->getCity() . "</Billing_City>\n";
                $xmlOutput = $xmlOutput . "<Billing_Province_Code/>\n";
                $xmlOutput = $xmlOutput . "<Billing_Province_Name>" . $billingAddress->getProvince() . "</Billing_Province_Name>\n";
                $xmlOutput = $xmlOutput . "<Billing_Country_Code>" . $billingAddress->getCountryId() . "</Billing_Country_Code>\n";

                $country = $om->create('\Magento\Directory\Model\CountryFactory')->create()->load($billingAddress->getCountryId());
                $xmlOutput = $xmlOutput . "<Billing_Country_Name>" . $country->getName() . "</Billing_Country_Name>\n";
            }


            // Dati Spedizione
            if(isset($shippingAddress) && !empty($shippingAddress)) {
            // ----------------------------------------
                $xmlOutput = $xmlOutput . "<Shipping_Reference>" . $shippingAddress->getLastname() . "|" . $shippingAddress->getFirstname() . "</Shipping_Reference>\n";
                $xmlOutput = $xmlOutput . "<Shipping_Name>" . $shippingAddress->getFirstname() . "</Shipping_Name>\n";
                $xmlOutput = $xmlOutput . "<Shipping_Surname>" . $shippingAddress->getLastname() . "</Shipping_Surname>\n";
                $xmlOutput = $xmlOutput . "<Shipping_Company/>\n";
                $xmlOutput = $xmlOutput . "<Shipping_Fiscal_Code/>\n";
                $xmlOutput = $xmlOutput . "<Shipping_Vat_Number/>\n";
                $xmlOutput = $xmlOutput . "<Shipping_Email>" . $order->getCustomerEmail() . "</Shipping_Email>\n";
                $xmlOutput = $xmlOutput . "<Shipping_Phone/>\n";
                $xmlOutput = $xmlOutput . "<Shipping_Mobile>" . $shippingAddress->getTelephone() . "</Shipping_Mobile>\n";
                $xmlOutput = $xmlOutput . "<Shipping_Address>" . $shippingAddress->getStreet()[0] . "</Shipping_Address>\n";
                $xmlOutput = $xmlOutput . "<Shipping_Zip>" . $shippingAddress->getPostcode() . "</Shipping_Zip>\n";
                $xmlOutput = $xmlOutput . "<Shipping_City>" . $shippingAddress->getCity() . "</Shipping_City>\n";
                
                $country = $om->create('\Magento\Directory\Model\CountryFactory')->create()->load($shippingAddress->getCountryId());

                if ($shippingAddress->getCountryId() == "IT") {
                    $xmlOutput = $xmlOutput . "<Shipping_Province_Code>" . $shippingAddress->getRegionCode() . "</Shipping_Province_Code>\n";
                    $xmlOutput = $xmlOutput . "<Shipping_Province_Name>" . $shippingAddress->getRegion() . "</Shipping_Province_Name>\n"; // <-- TODO: Manca
                }
                else {
                    $xmlOutput = $xmlOutput . "<Shipping_Province_Code/>\n";
                    $xmlOutput = $xmlOutput . "<Shipping_Province_Name>" . $shippingAddress->getCity() . "</Shipping_Province_Name>\n"; // <-- TODO: Manca
                }

                
                $xmlOutput = $xmlOutput . "<Shipping_Country_Code>" . $shippingAddress->getCountryId() . "</Shipping_Country_Code>\n";
                $xmlOutput = $xmlOutput . "<Shipping_Country_Name>" . $country->getName() . "</Shipping_Country_Name>\n";

            }

            if (isset($billingAddress) && !empty($billingAddress) && $billingAddress->getTaxCode() != "" && $billingAddress->getTaxCode() != "-") {
                $xmlOutput = $xmlOutput . "<Order_Invoice_Request>True</Order_Invoice_Request>\n";
            }
            else {
                $xmlOutput = $xmlOutput . "<Order_Invoice_Request>false</Order_Invoice_Request>\n";
            }


            // Articoli
            // ----------------------------------------
            $xmlOutput = $xmlOutput . "<Rows>\n";
            $orderItems = $order->getAllVisibleItems();
            $i = 0;

            $products_quantity = 0;

            foreach ($orderItems as $item) {
                $i++;
                $products_quantity = $products_quantity + intval($item->getQtyOrdered());
                $xmlOutput = $xmlOutput . "<Row>\n";
                $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>" . $item->getSku() . "</Order_Row_Item_Code>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Description>" . $item->getTitle() . "</Order_Row_Description>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Quantity>" . intval($item->getQtyOrdered()) . "</Order_Row_Quantity>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Price>" . str_replace(".", ",", $item->getPrice('final_price')."") . "</Order_Row_Price>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>false</Order_Row_Free_Sample>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
                $xmlOutput = $xmlOutput . "</Row>\n";
            }

            
            // PROMOZIONI
            // ----------------------------------------
            // Coupon
            if (!empty($order->getCouponCode())) {
                // $ruleId =  $order->getAppliedRuleIds()[0];
                
                // $rule = $om->create('Magento\SalesRule\Model\Rule')->load($ruleId); //Shopping Cart Rule
                $couponCode = $order->getCouponCode();
                if ( strtolower($couponCode) == strtolower("BEAUTYSTUDIO") ) {
                    $i++;
                    $xmlOutput = $xmlOutput . "<Row>\n";
                    $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>58PO05</Order_Row_Item_Code>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Description>Zainetto Backpack</Order_Row_Description>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Quantity>1</Order_Row_Quantity>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Price>0,0</Order_Row_Price>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>True</Order_Row_Free_Sample>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
                    $xmlOutput = $xmlOutput . "</Row>\n";
                }
                
                if ( strtolower($couponCode) == strtolower("SUPERCHERRYMARTY") ) {
                    # Se c'è un impeccable e ci sono almeno 2 prodotti
                    #
                    # SUPERCHERRYMARTY si avrà in omaggio due mini taglie, rispettivamente quelle di crema mani e struccante micellare.
                    # 
                    # 589002 - Delicate Micellar Make-Up Remover 50 ml
                    # 589102 - Mini Pink Butter Nourishing Hand & Nail Cream 30 ml
                    $i++;
                    $xmlOutput = $xmlOutput . "<Row>\n";
                    $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>589002</Order_Row_Item_Code>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Description>Delicate Micellar Make-Up Remover 50 ml</Order_Row_Description>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Quantity>1</Order_Row_Quantity>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Price>0,0</Order_Row_Price>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>True</Order_Row_Free_Sample>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
                    $xmlOutput = $xmlOutput . "</Row>\n";

                    $i++;
                    $xmlOutput = $xmlOutput . "<Row>\n";
                    $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>589102</Order_Row_Item_Code>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Description>Mini Pink Butter Nourishing Hand & Nail Cream 30 ml</Order_Row_Description>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Quantity>1</Order_Row_Quantity>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Price>0,0</Order_Row_Price>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>True</Order_Row_Free_Sample>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
                    $xmlOutput = $xmlOutput . "</Row>\n";
                }
                else if ( strtolower($couponCode) == strtolower("SUPERCHERRY") && 
                        $products_quantity > 1 ) {
                    $i++;
                    $xmlOutput = $xmlOutput . "<Row>\n";
                    $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>58PO03</Order_Row_Item_Code>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Description>Pochette</Order_Row_Description>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Quantity>1</Order_Row_Quantity>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Price>0,0</Order_Row_Price>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>True</Order_Row_Free_Sample>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
                    $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
                    $xmlOutput = $xmlOutput . "</Row>\n";
                }
            }

            // if (sizeof($orderItems) > 1) { 
            // date_default_timezone_set("Europe/Rome");
            // if (date('Y-m-d H:i') > '2018-11-09 14:00' && 
            //     date('Y-m-d H:i') < '2018-11-11 23:59' && 
            //     ($order->getGrandTotal() - $order->getShippingAmount()) > 29.00) {
            //     // && sizeof($orderItems) > 1) {
            //     // Logica: per ogni ordine di almeno 2 pezzi.
            //     // Dovrà essere attivo da Venerdì 9 ore 14:00 fino a Domenica 11 ore 23:59
            //     // Pochette Single Weekend 599914
            //     $i++;
            //     $xmlOutput = $xmlOutput . "<Row>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>599914</Order_Row_Item_Code>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Description>Pochette Single Weekend</Order_Row_Description>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Quantity>1</Order_Row_Quantity>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Price>0,0</Order_Row_Price>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>True</Order_Row_Free_Sample>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
            //     $xmlOutput = $xmlOutput . "</Row>\n";
            // }


            // CAMPIONCINI
            // ----------------------------------------
            if ($products_quantity > 5) { 
                // Per ordini con più di 5 prodotti in omaggio la confezione regalo
                $i++;
                $xmlOutput = $xmlOutput . "<Row>\n";
                $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>599906</Order_Row_Item_Code>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Description>Confezione regalo</Order_Row_Description>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Quantity>1</Order_Row_Quantity>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Price>0,0</Order_Row_Price>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>True</Order_Row_Free_Sample>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
                $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
                $xmlOutput = $xmlOutput . "</Row>\n";
            }


            // if ($order->getSampleForm() != null && $order->getSampleForm() != "") { 
            //     // Campioncino omaggio - Scelto dall'utente
            //     $i++;
            //     $xmlOutput = $xmlOutput . "<Row>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>" . $order->getSampleForm() . "</Order_Row_Item_Code>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Description>Campioncino omaggio</Order_Row_Description>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Quantity>1</Order_Row_Quantity>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Price>0,0</Order_Row_Price>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>True</Order_Row_Free_Sample>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
            //     $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
            //     $xmlOutput = $xmlOutput . "</Row>\n";
            // }
            // else {

            // 581708M Lasting Embrace Lip Colour 08 Rosso Rubino, Rossetto Liquido Lunga Tenuta
            $i++;
            $xmlOutput = $xmlOutput . "<Row>\n";
            $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>581708M</Order_Row_Item_Code>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Description>Lasting Embrace Lip Colour 08 Rosso Rubino</Order_Row_Description>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Quantity>1</Order_Row_Quantity>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Price>0,0</Order_Row_Price>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>True</Order_Row_Free_Sample>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
            $xmlOutput = $xmlOutput . "</Row>\n";


            // 59CC04 Adesivi con codice sconto
            $i++;
            $xmlOutput = $xmlOutput . "<Row>\n";
            $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>59CC04</Order_Row_Item_Code>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Description>Adesivi con codice sconto</Order_Row_Description>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Quantity>1</Order_Row_Quantity>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Price>0,0</Order_Row_Price>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>True</Order_Row_Free_Sample>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
            $xmlOutput = $xmlOutput . "</Row>\n";
            
            // 58BC01 Busta e bigliettino di ringraziamento
            $i++;
            $xmlOutput = $xmlOutput . "<Row>\n";
            $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>58BC01</Order_Row_Item_Code>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Description>Busta e bigliettino di ringraziamento</Order_Row_Description>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Quantity>1</Order_Row_Quantity>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Price>0,0</Order_Row_Price>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>True</Order_Row_Free_Sample>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
            $xmlOutput = $xmlOutput . "</Row>\n";

            // Campioncino omaggio - Campioncino Blooming BB Foundation
            $i++;
            $xmlOutput = $xmlOutput . "<Row>\n";
            $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>590503C</Order_Row_Item_Code>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Description>Campioncino Blooming BB Foundation</Order_Row_Description>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Quantity>1</Order_Row_Quantity>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Price>0,0</Order_Row_Price>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>True</Order_Row_Free_Sample>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
            $xmlOutput = $xmlOutput . "</Row>\n";
            
            
            // Campioncino omaggio - Campioncino Blur Me Face Primer
            $i++;
            $xmlOutput = $xmlOutput . "<Row>\n";
            $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>590101C</Order_Row_Item_Code>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Description>Campioncino Blur Me Face Primer</Order_Row_Description>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Quantity>1</Order_Row_Quantity>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Price>0,0</Order_Row_Price>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>True</Order_Row_Free_Sample>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
            $xmlOutput = $xmlOutput . "</Row>\n";


            // Campioncino omaggio - Etichetta chiusura pacco
            $i++;
            $xmlOutput = $xmlOutput . "<Row>\n";
            $xmlOutput = $xmlOutput . "<Order_Number>" . $order_prefix . $order->getRealOrderId() . "</Order_Number>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Number>" . $i . "</Order_Row_Number>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Status>false</Order_Row_Status>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Item_Code>58E697</Order_Row_Item_Code>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Description>Etichetta chiusura pacco</Order_Row_Description>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Quantity>1</Order_Row_Quantity>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Quantity_Shipped>0</Order_Row_Quantity_Shipped>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Price>0,0</Order_Row_Price>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Discount>0,0</Order_Row_Discount>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Discount_Value>0,0</Order_Row_Discount_Value>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample>True</Order_Row_Free_Sample>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Code/>\n";
            $xmlOutput = $xmlOutput . "<Order_Row_Free_Sample_Name/>\n";
            $xmlOutput = $xmlOutput . "</Row>\n";

                
            // }
                
            $xmlOutput = $xmlOutput . "</Rows>\n";

            $xmlOutput .= "</Order>\n";
            $xmlOutput .= "</Orders>\n";
            $filename = $order_prefix . $order->getRealOrderId() . ".xml";
            
          //  echo $order->getRealOrderId() ;
			//echo "<br>";
            // Upload ordini
            // ----------------------------------------
            $this->uploadFile($conn_id, $order, $filename, $xmlOutput); // <-- Carico il file su ftp
        }  
              

        // ----------------------------------------
        // ORDER RECEIVING
        // ----------------------------------------
        $this->readFiles($conn_id);

        // Chiudo la connessione
        ftp_close($conn_id);
    }


    public function sendInvoiceEmail($to, $storeId) {
        $templateOptions = array('area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId);
        $templateVars = array( 
                                'message' => 'Hello World!!.', // TODO: username
                            );
        $from = "marcofiore1983@gmail.com";
        $to = array($to);


        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $filesystem = $om->get('Magento\Framework\Filesystem');
        $writer = $filesystem->getDirectoryWrite('tmp');
        $directoryList = $om->get('\Magento\Framework\Filesystem\DirectoryList');
        $pdfFile =  $directoryList->getPath('tmp') . "/text_graphic_image.pdf";

        $mail = new Zend_Mail('utf-8');
        $mail->setBodyHtml("Ok, funziona")
             ->setSubject('Naj Oleari Beauty Fattura')
             ->addTo($to)
             ->setFrom($from);

        $content = file_get_contents($pdfFile);
        $attachment = new Zend_Mime_Part($content);
        $attachment->type = 'application/pdf';
        $attachment->disposition = Zend_Mime::DISPOSITION_INLINE;
        $attachment->encoding = Zend_Mime::ENCODING_BASE64;
        $attachment->filename = 'text_graphic_image.pdf'; // name of file

        $mail->addAttachment($attachment);

        try {
            $ok = $mail->send();
        } 
        catch (Exception $e) {
            Mage::logException($e);
        }        
    }

    public function sendTrackingCodeEmail($to, $storeId, $trackingMessage) {
        $templateOptions = array('area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId);
        $from = "marcofiore1983@gmail.com";
        $to = array($to);

        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $filesystem = $om->get('Magento\Framework\Filesystem');
        $writer = $filesystem->getDirectoryWrite('tmp');
        $directoryList = $om->get('\Magento\Framework\Filesystem\DirectoryList');
        $pdfFile =  $directoryList->getPath('tmp') . "/text_graphic_image.pdf";

        
        $mail = new Zend_Mail('utf-8');
        $mail->setBodyHtml($trackingMessage)
             ->setSubject('Naj Oleari Beauty - Ordine spedito')
             ->addTo($to)
             ->setFrom($from);

        try {
            $ok = $mail->send();
        } 
        catch (Exception $e) {
            Mage::logException($e);
        }        
    }


    public function readFiles($conn_id) {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $filesystem = $om->get('Magento\Framework\Filesystem');
        $reader = $filesystem->getDirectoryWrite('tmp');

        // Array dei nomi dei file ricevuti
        $fileNames = array();

        // Entro nella cartella receive (../ perché prima ero nella /send)
        if (ftp_chdir($conn_id, "receive")) { 
            // Itero sui file della cartella
            $files = ftp_nlist($conn_id, ".");
            
            foreach ($files as $filename) {
                if ($filename != "." && $filename != "..") {
                    // Aggiungo i nomi dei file all'array 
                    array_push($fileNames, $filename);
                }
            }
        }

        $directoryList = $om->get('\Magento\Framework\Filesystem\DirectoryList');                
        // Itero i file remoti 
        foreach ($fileNames as $filename) {
            // e li salvo nella tmp 
             $localFilePath =  $directoryList->getPath('tmp') . "/" . $filename;
			
            ftp_get($conn_id, $localFilePath, $filename, FTP_ASCII);

            $fileContents = file_get_contents($localFilePath);
            $pattern = '/\\x1a/';
            $fileContents = preg_replace($pattern, "'", $fileContents);
            
            // Converto l'xml
            $use_errors = libxml_use_internal_errors(true);
            $xml = simplexml_load_string($fileContents);
            if (false === $xml) {
                // skip file
                continue;
            }
            libxml_clear_errors();
            libxml_use_internal_errors($use_errors);
        
            foreach($xml->Order as $ftpOrder) {
                // e leggo i dati
                $storeId = '1';
                $orderId = str_replace("IT", "", $ftpOrder->Order_Number);
                if (substr($ftpOrder->Order_Number, 0, 2) === "EN") {
                    $storeId = '2';
                    $orderId = str_replace("EN", "", $orderId);
                }
                
                /** @var \Magento\Framework\App\ObjectManager $om */
                $om = \Magento\Framework\App\ObjectManager::getInstance();
                /** @var \Magento\Sales\Model\ResourceModel\Product\CollectionFactory $orderCollection */
                $orderCollection = $om->create('Magento\Sales\Model\ResourceModel\Order\CollectionFactory');

                $collection = $orderCollection->create()
                                              ->addFieldToFilter('store_id', $storeId)
                                              ->addFieldToFilter('increment_id', $orderId);
                                            // ->addFieldToFilter('increment_id', '000000046'); // <-- Test
                
                if ($collection->count() > 0) {
                    $order = $collection->getFirstItem();
                    
                    $order->setSdaTrackingNumber("https://www.poste.it/cerca/index.html#/risultati-spedizioni/". $ftpOrder->Tracking_Number)
                          ->setState("shipped")
                          ->setStatus("shipped");
                    $order->save();


                    // Impostare spedizione
                    // --------------------
                    // $convertOrder = $this->_objectManager->create('Magento\Sales\Model\Convert\Order');
                    // $shipment = $convertOrder->toShipment($order);

                    // $data = array(
                    //     'carrier_code' => 'SDA',
                    //     'title' => 'SDA',
                    //     'number' => 'TORD23254WERZXd3', // Replace with your tracking number
                    // );

                    // // Loop through order items
                    // foreach ($order->getAllVisibleItems() AS $orderItem) {
                    //     // Check if order item is virtual or has quantity to ship
                    //     if (! $orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                    //         continue;
                    //     }    
                    //     $qtyShipped = $orderItem->getQtyToShip();
                        
                    //     // Create shipment item with qty
                    //     $shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);

                    //     // Add shipment item to shipment
                    //     $shipment->addItem($shipmentItem);
                    // }

                    // try {
                    //     // Save created shipment and order
                    //     $track = $om->create('Magento\Sales\Model\Order\Shipment\TrackFactory')->create()->addData($data);
                    //     $shipment->addTrack($track)->save();
                    //     $shipment->save();
                    //     $shipment->getOrder()->save();
                    
                    //     // Send email
                    //     // TODO
                    //     // $this->_objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
                    //     //                     ->notify($shipment);
                    
                    //     $shipment->save();
                    // } catch (\Exception $e) {
                    //     throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
                    // }
                }        
            }

            // Caricamento su S3
            // --------------------
            // $cpS3 = "sudo -u webapp aws s3 cp " . $localFilePath . " s3://najoleari-invoices/invoices" . $filename;
            // // $output = shell_exec($cpS3);
            // echo "<pre>".$cpS3."</pre>";

            // Elimino il file temp dalla directory
            if (file_exists($localFilePath)) {
                unlink($localFilePath);
            }
            
            // Elimino il file da ftp
            ftp_delete($conn_id, $filename);
        }                
    }


    public function uploadFile($conn_id, $order, $filename, $xmlOutput) {
        
        $writerlog = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
$logger = new \Zend\Log\Logger();
$logger->addWriter($writerlog);
        
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $filesystem = $om->get('Magento\Framework\Filesystem');
        $writer = $filesystem->getDirectoryWrite('tmp');
        //$logger->info('write directory => '.$writer);
        $file = $writer->openFile($filename, 'w'); 
        try {
            $file->lock();
            try {
                $file->write($xmlOutput);
            }
            finally {
                $file->unlock();
            }
        }
        finally {
            $file->close();
        }

        $directoryList = $om->get('\Magento\Framework\Filesystem\DirectoryList');
         $localFilePath =  $directoryList->getPath('tmp') . "/" . $filename;
		
        $remoteFilePath = "as400/" . $filename;
                
        // upload the file
        if (ftp_put($conn_id, $remoteFilePath, $localFilePath, FTP_ASCII)) {
            $order_prefix = ($order->getStoreId() == 2) ? "EN" : "IT";
            $state = $om->get('Magento\Framework\App\State');
            
            $file = null;
            if ($state->getMode() == \Magento\Framework\App\State::MODE_PRODUCTION) {
                $file = $writer->openFile("as400-prod.log", 'a');
            }
            else {
                $file = $writer->openFile("as400-dev.log", 'a');
            }
            try {
                $file->lock();
                try {                    
                    $file->write("BEFORE --> " . date("d/m/Y H:i:s") . " - " . $order_prefix . $order->getRealOrderId() . " - " . $order->getStatus() . "\n" );
                }
                finally {
                    $file->unlock();
                }
            }
            finally {
                $file->close();
            }
            

            // Aggiorno lo stato ordine --> In Laborazione, preso in carico da AS400
            $order->setState("processing")
                  ->setStatus("as_400");
            $order->save();

            if ($state->getMode() == \Magento\Framework\App\State::MODE_PRODUCTION) {
                $file = $writer->openFile("as400-prod.log", 'a');
            }
            else {
                $file = $writer->openFile("as400-dev.log", 'a');
            }
            try {
                $file->lock();
                try {
                    $file->write("AFTER --> " . date("d/m/Y H:i:s") . " - " . $order_prefix . $order->getRealOrderId() . " - " . $order->getStatus() . "\n" );
                }
                finally {
                    $file->unlock();
                }
            }
            finally {
                $file->close();
            }

            // Elimino il file temp dalla directory
            unlink($localFilePath);
        } 
    }
}
?>
